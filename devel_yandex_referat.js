;(function ($) {
  Drupal.behaviors.develYandexReferatRussianNames = {
    attach: function(context, settings) {
      $('.fields-russian-names-table', context).once('devel-yandex-referat', function () {
        var table = this;

        $('input[type=hidden][value=0]').closest('tr').removeClass('js-hidden');

        $('.fields-add-more', table).click(function() {
          $('tr.js-hidden:first', table)
            .removeClass('js-hidden')
            .find('input[type=hidden]:first')
            .val(0);

          if (!$('tr.js-hidden:first', table).length) {
            $(this).parent().hide();
          }
        });

        $('.fields-hide', table).click(function() {
          $(this).closest('tr').addClass('js-hidden')
                 .find('input[type=hidden]:first')
                 .val(1);

          if ($('tr.js-hidden:first', table).length) {
            $('.fields-footer-row', table).show();
          }
        });

      });
    }
  };
})(jQuery);
