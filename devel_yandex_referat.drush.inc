<?php
/**
 * @file
 *  Drush integration for 'generate-content' commant.
 */

function devel_yandex_referat_drush_help_alter(&$command) {
  if ($command['command'] == 'generate-content') {
    $command['options']['ya'] = 'Use Yandex.Referats as content creator.';
  }
}
