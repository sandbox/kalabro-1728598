<?php
/**
 * @file
 * Class for requesting Yandex.Referats
 *
 */

class YandexReferat {
  // Title form Referat
  protected $title;
  // Body from Referat
  protected $body;
  // Topics from Referat
  protected $topics;
  // Topic settings
  protected $userTopics;
  // Built query to Yandex.Referats
  private $query;
  // Avalible topics for service Yandex.Referats
  protected static $avalibleTopics = array(
    'astronomy'    => 'астрономия',
    'geology'      => 'геология',
    'gyroscope'    => 'гироскопия',
    'literature'   => 'литературоведение',
    'marketing'    => 'маркетинг',
    'music'        => 'музыковедение',
    'polit'        => 'политология',
    'agrobiologia' => 'почвоведение',
    'law'          => 'правоведение',
    'psychology'   => 'психология',
    'geography'    => 'страноведение',
    'physics'      => 'физика',
    'philosophy'   => 'философия',
    'chemistry'    => 'химия',
    'estetica'     => 'эстетика',
  );
  // Avalible hosts for service Yandex.Referats
  private static $hosts = array(
    'http://www.referats.yandex.ru',
    'http://vesna.yandex.ru'
  );

  public function __construct(array $user_topics = array()) {
    $this->userTopics = $user_topics;
    $this->prepareQuery();
  }

  function __get($name) {
    if (isset($this->{$name})) {
      return $this->{$name};
    }
  }

  protected function prepareRandomTopics() {
    if (empty($this->userTopics)) {
      $topics = self::$avalibleTopics;
    }
    else {
      $topics = $this->userTopics;
    }

    // Random count of topics.
    $max_cnt = count($topics);
    $cnt = rand(1, $max_cnt);

    $this->topics = (array)array_rand($topics, $cnt);
    return $this->topics;

  }

  /**
   * query example:
   * http://referats.yandex.ru/referats/write/?t=astronomy+estetica
   */
  protected function prepareQuery() {
    $query = '';

    $topics = $this->prepareRandomTopics();
    // Get one of two avalible hosts.
    $host = self::$hosts[ rand(0, 1) ];
    $query = $host . '/referats/write/?t=' . implode('+', $topics);

    $this->query = $query;
    return $this->query;
  }

  public function request($reset = FALSE) {
    if ($reset) {
      $this->prepareQuery();
    }

    $result = drupal_http_request($this->query);
    // I'm sorry. We need some delay.
    $delay = variable_get('devel_yandex_referat_delay_mks', 100000);
    usleep($delay);
    if (empty($result->error)) {
      // Faster then preg_match().
      $doc = new DOMDocument();
      // DOMDocument wants ISO-8859-1. But we have utf-8. Fast fix.
      if (@$doc->loadHTML('<?xml encoding="utf-8" ?>' . $result->data)) {
        $xpath = new DOMXpath($doc);
        $headerNodes = $xpath->query('//strong[1]');
        $this->title = $headerNodes->item(0)->nodeValue;
        // Trim quotes.
        if (preg_match('/«(.*)»/iu', $this->title, $matches)) {
          $this->title = $matches[1];
        }

        // Array of paragraphs
        $this->body = array();
        $paragraphNodes = $xpath->query('//p');
        foreach ($paragraphNodes as $Node) {
          $this->body[] = str_replace(array("\r", "\r\n", "\n"), '', $Node->nodeValue);
        }
        return TRUE;

      }
      else {
        watchdog('yandex_referat', 'Error while parsing HTML for URL: %url', array('%url' => $this->query), WATCHDOG_ERROR);
      }
    }
    else {
      watchdog('yandex_referat', 'Error "%error" (@code) for URL: %url',
              array('%error' => $result->error, '@code' => $result->code, '%url' => $this->query),
              WATCHDOG_ERROR
            );
    }

    return FALSE;
  }

  public static function getAvalibleTopics($topic_key = NULL) {
    if (!isset($topic_key)) {
      return self::$avalibleTopics;
    }
    elseif (isset(self::$avalibleTopics[$topic_key])) {
      return self::$avalibleTopics[$topic_key];
    }

    return FALSE;

  }
}
